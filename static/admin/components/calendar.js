export default {
  // Internal id of the component
  id: "calendar-widget",
  // Visible label
  label: "Calendar Widget",
  // Fields the user need to fill o ut when adding an instance of the component
  fields: [
    {
      name: "id",
      label: "Calendar ID for group",
      widget: "string",
    },
    {
      name: "name",
      label: "Calendar name for group",
      widget: "string",
    },
  ],
  // Regex pattern used to search for instances of this block in the markdown document.
  pattern: /^{{< calendar (.+) (.+) >}}/,
  // This is used to populate the custom widget in the markdown editor in the CMS.
  fromBlock: function (match) {
    return {
      id: match[1],
      name: match[2],
    };
  },
  // This is used to serialize the data from the custom widget to the
  // markdown document
  toBlock: function (data) {
    return `{{< calendar ${data.id} ${data.name} >}}`;
  },
  // Preview output for this component. Can either be a string or a React component
  // (component gives better render performance)
  toPreview: function (data) {
    var name;
    switch (data.name) {
      case "net":
        name = "Net";
        break;
      case "bar":
        name = "Bar";
        break;
      default:
        name = "Beboerr%C3%A5det";
        break;
    }
    return `
      <iframe
      src="https://calendar.zoho.eu/zc/ui/embed/#calendar=${data.id}&title=${name}&type=1&language=en&timezone=Europe%2FCopenhagen&showTitle=0&showTimezone=0&startingDayOfWeek=0&view=month&showDetail=0&theme=1&showAttendee=0&showSwitchingViews=0&showAllEvents=0&showLogo=0"
      width=500 height=500 frameBorder="0" scrolling="no">
      </iframe>
      `;
  },
};
