---
title: "Update: Canceled events in March"
date: 2020-03-13
author: ABK-Aalborg
tags:
  - Event
---

A few events are canceled in March:

- Beer pong tournament the 13th (2020-03-13)
- Aloha block party / Kro Aften the 28th (2020-03-28)

Update:
- [ABK-NET general assembly](https://abk-aalborg.dk/post/abk-net-assembly-2020/) is postponed to an unknown time.


Read more about why, [here](https://politi.dk/coronavirus-i-danmark/in-english).
