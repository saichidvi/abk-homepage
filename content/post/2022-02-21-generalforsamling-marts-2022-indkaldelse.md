---
title: Generalforsamling marts 2022 indkaldelse
date: 2022-02-21T18:50:18.406Z
author: ABK-NET
tags:
  - ABK-NET
  - Event
attachment: /media/generalforsamling_marts_2022_indkaldelse.pdf
---

Se indkaldelsen i den vedhæftede pdf-fil.

See the invitation in the attached pdf-file.
