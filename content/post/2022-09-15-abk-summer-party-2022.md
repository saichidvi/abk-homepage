---
title: ABK SUMMER PARTY 2022
subtitle: ""
date: 2022-09-15T09:03:12.024Z
author: ABK-Bar
tags:
  - Event
---
![ABK Summer Party-poster](/media/sommerfest-feed.jpg)

[TILMELDING VIA GOOGLE FORMS INDEN D. 21 SEPTEMBER](https://docs.google.com/forms/d/e/1FAIpQLSeR5SihjVxhhjK0mYa2XngBt2ieWkIRwPVSQXEFmRXX1NZJLw/viewform)

Semesteret er begydt igen, sommeren er ved at være ovre og de mørke dage banker på døren.\
MEN inden vi siger helt farvel til sommeren, så skal vi selvfølgelig have en sommerfest!\
ABK inviterer derfor til sommerfest d. 24. september kl. 15:00 i fælleshuset. Find dit gode humør frem, tag dine naboer i hånden og stift bekendtskab med dine medkollegianere.

Festen starter kl. 15:00, hvor der vil være nogle sjove aktiviteter til at sætte gang i festen. Det er selvfølgelig frivilligt, men vi opfordrer alle til at deltage i dem!

Når vi nærmere os aftenen, vil der blive tændt op i grillen! ABK-baren sørger for salater og brød, så det eneste du/I skal medbringe er kød (eller andet) til grillen. Det er ikke muligt at opbevare maden i fælleshuset inden aftensmaden, hvorfor der vil blive afsat tid til afhentning af maden i egen lejlighed efter aktiviteterne.

Baren vil være åben hele aftenen, så der vil være mulighed for at købe øl, sodavand og drinks!

Gæster udefra er velkomne fra kl. 20:30![](https://docs.google.com/forms/d/e/1FAIpQLSeR5SihjVxhhjK0mYa2XngBt2ieWkIRwPVSQXEFmRXX1NZJLw/viewform)

[Tilmeldingen sker via Google Forms inden d. 21. september](https://docs.google.com/forms/d/e/1FAIpQLSeR5SihjVxhhjK0mYa2XngBt2ieWkIRwPVSQXEFmRXX1NZJLw/viewform)

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

[SIGN-UP THROUGH THE LINK BEFORE THE 21st OF SEPTEMBER](https://docs.google.com/forms/d/e/1FAIpQLSeR5SihjVxhhjK0mYa2XngBt2ieWkIRwPVSQXEFmRXX1NZJLw/viewform)

On September the 24st, this year’s summer party takes place!\
So, gather your neighbors and come meet your fellow residents.\
The party starts at 15:00. Here there will be some fun activities to get the party started. We encourage everyone to participate in the fun activities we have planned!

In the evening, we eat together, and the grill will be warm. We provide salads and bread, so you only have to bring meat (or something else) for the grill. The grill will be ready at 19:00. It is not possible to store your food in the common house before dinner, which is why time will be set aside for getting your food in your own apartment after the activities

The bar will be open the entire evening, so you can buy beer, drinks and soda!

Guests not living in the dorm are welcome from 20:30[!](https://docs.google.com/forms/d/e/1FAIpQLSeR5SihjVxhhjK0mYa2XngBt2ieWkIRwPVSQXEFmRXX1NZJLw/viewform)

[The sing-up is done via Google Forms before 21st of September](https://docs.google.com/forms/d/e/1FAIpQLSeR5SihjVxhhjK0mYa2XngBt2ieWkIRwPVSQXEFmRXX1NZJLw/viewform)