---
title: Motionsrummet åbner igen 1. September
date: 2021-09-03T06:02:32.093Z
author: Administration / Caretaker
tags:
  - Facilities
---

Velkommen tilbage 

Husk fortsat de gode vaner.

Hold afstand.

Bliv hjemme, hvis du er syg.

Af sprit håndtag og greb efter øvelser.

Hold 2 meters afstand i frivægts- og tårnområder.



Keep in mind the good habits.

Keep distance.

Stay home if you are ill.

Clean handles and grips after exercises.

Keep 2 meters away in free weight and tower areas
