---
title: ABK-NET General Assembly
subtitle: Drill only between 9:00 and 19:00
date: 2020-03-03
author: ABK-NET
tags:
  - ABK-NET
  - Event
---

ABK-Net board are the people who provide you and everyone else at ABK internet for 25 kr/month!
To continue to deliver cheap internet we need new volunteers to help develop and maintain the network here at ABK.
We are a group of volunteers from ABK who meet the first Tuesday every month to work on the network and website.
Besides that we have nice workdays, with hacking, fun and free pizza.
We have a good economy, which provides a good opportunity to be part of exciting projects. 
If you have any questions, don’t hesitate to ask us at sysadm@abk-aalborg.dk!

```
 __________ 
< Join us! >
 ---------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

# Agenda

- Pizza ordering paid by us!
- Choice of conductor and minute take. 
- Presentation of ABK-Net
- Account of the year and budget
- Change of statutes (to find at abk-aalborg.dk the week before)
- Election for the board
- Other matters
- Pizza eating and soft drinks

# Date and Time

Tuesday the 24th of March at 19 o'clock (2020-03-24T19:00) in the common room.
