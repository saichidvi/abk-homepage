---
title: Generalforsamling i ABK-Baren
subtitle: 07-09-2021
date: 2021-08-30T08:36:19.188Z
author: ABK-Bar
tags:
  - ABK-Bar
  - Event
---

## Danish

ABK-Baren indkalder til generalforsamling d. 7. september 2021 kl. 20 i fælleshuset!

ABK-Baren er ABK’s “festudvalg” og arrangør af fredagsbarer og andre sociale events. Vi er drevet 100% af frivillige kræfter fra kollegiet, og vi kan derfor altid bruge din hjælp.

Efter en lang nedlukning i forbindelse med Covid-19 skal der gøres klar til, at fredagsbaren igen kan åbne døren til billige drikkevarer, musik, dans, beer-pong og ikke mindst masser af hygge!

Dagsordenen til mødet er følgende:

1. Valg af dirigent
2. Valg af referent
3. Hvem er ABK-Baren?
4. Beretning fra formanden
5. Beretning fra kasseren
6. Valg af bestyrelse
7. Indkomne forslag
8. Eventuelt

Hvis du har forslag til ændringer i dagsordenen eller generelle forslag til fredagsbaren eller sociale arrangementer, så læg en seddel i postkassen hos Mathias Andresen (blok 7, lejl. 3) eller skriv en mail til bar@abk-aalborg.dk.



## English

ABK-Baren General Meeting at the Common House, September 7, at 20:00.

ABK-Baren is ABK's "party committee" and organizer of Friday bars and other social events. We are driven 100% by volunteers from the dorm, and we can always use your help.

After a long lockdown in connection with Covid-19, preparations must be made for the Friday bar to reopen its door to cheap drinks, music, dance, beer-pong and lots of fun!

The agenda for the meeting is as follows:

1. Election of conductor
2. Election of rapporteur
3. What is the ABK-Baren?
4. Report from the chairman
5. Report from the cashier
6. Election of the Board
7. Received proposals
8. Any other business

If you have suggestions for changes to the agenda or general suggestions for the Friday bar or other social events, put a note in the mailbox at Mathias Andresen (block 7, apartment 3) or write an email to bar@abk-aalborg.dk.
