---
title: A little about heat in the radiators
date: 2021-09-24T08:59:00.105Z
author: Administration / Caretaker
tags:
  - Bathroom Renovation
---

The radiator system starts up automatically depending on the outdoor temperature.

The system only starts when the outside temperature is constantly below 20 degrees. During the weekend and the first days of this week, the temperature was down to 10 degrees over several days and then the plant starts.

At the same time in block 3, the radiators are often shut off, and the water drained as they dismantle the radiator and switch on the underfloor heating in the bathroom in connection with the bathroom renovation.

Hope for understanding that it will give some problems some time yet.

Sincerely

Hans
