---
title: SMS-Service
date: 2021-08-30T09:31:26.166Z
author: Administration / Caretaker
tags:
  - Service Announcement
---

A smart way to stay informed

Notification/ info is sent via SMS, e.g. before planned water outage or in case of acute damage.

Sign up for SMS service can be done at Hasseris Boligselskabs website.

[www.hasseris-boligselskab.dk/beboer/til-og-frameld-sms-service/](https://www.hasseris-boligselskab.dk/beboer/til-og-frameld-sms-service/)
