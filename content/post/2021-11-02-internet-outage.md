---
title: Internet outage
subtitle: ""
date: 2021-11-02T19:37:56.875Z
author: ABK-NET
tags:
  - Service Announcement
---

We apologize for the short outage this evening. More might occur, as we are changing some configurations in the server room. 

We apologize for any inconvenience this may cause.
