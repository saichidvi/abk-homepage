---
title: Internet scheduled to be down for maintanance
subtitle: Maintanance scheduled for monday 3/5 between 7:30 pm and 9 pm 
date: 2021-04-27
author: ABK-NET
tags:
  - Service Announcement
---

Next Monday 3/5 between 7:30 pm and 9 pm (if everything goes according to plan), there will be interruptions in your internet connection as we, among other things, need to replace a malfunctioning fan in a switch in block 3. 


We will try to minimize the downtime of the internet.
