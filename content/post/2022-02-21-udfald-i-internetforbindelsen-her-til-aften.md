---
title: Udfald i internetforbindelsen her til aften
date: 2022-02-21T19:15:49.325Z
author: ABK-NET
tags:
  - Service Announcement
---

Der vil forekomme udfald i nettet her til aften, da vi arbejder på de sidste detaljer ifbm. jeres 1Gbit forbindelse. Udfaldene vil starte fra kl. 20:45. 

The internet connection will be unstable this evening, as we are working on the final changes with regard to your 1Gbit connection. The internet will cut out temporarily at 20:45.
