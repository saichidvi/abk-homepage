---
title: Update in Fitness room
subtitle:
date: 2019-05-20
tags:
  - Facilities
  - ResidentsCouncil
---

 New additions: jack cable, bar bells (2x 30 kg, 2x 34 kg and 2x 38 kg), two weight lift poles (handle up to 650 kg), one step board (3 height of config), one aerobic set, 6x fitness elastic in different strength and some quick lock for the weight lift poles. 
