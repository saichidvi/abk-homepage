---
title: Planned internet maintenance
subtitle: Planned internet maintenance on Monday the 17th of January 2022
date: 2022-01-15T09:52:27.870Z
author: ABK-NET
tags:
  - Service Announcement
---

There will be periodical outages of the internet on Monday 17th between 19:00 and 21:30.

Without going into too much technical detail, this happens because we have to integrate the last component of our new internet setup.

We apologize for any inconvenience this may cause.
