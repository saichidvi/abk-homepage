---
title: Indkaldelse til bestyrelsesmøde i ABK-Net
subtitle: Kun for ABK-Nets nyvalgte bestyrelsesmedlemmer
date: 2022-03-14T20:39:52.116Z
author: ABK-NET
tags:
  - ABK-NET
attachment: /media/agenda_213-2022_board_meeting.pdf
---

Mødet finder sted mandag d. 21/3-2022 kl. 18:30 i Fælleshuset.

Agendaen er vedhæftet her.
