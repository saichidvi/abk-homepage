---
title: Bathroom Renovation
---

[{{< mdi image >}} Illustration](/pdf/bathroom-renovation-Illustration-ABK.pdf)
[{{< mdi file-pdf >}} Folder (dansk)](/pdf/bathroom-renovation-Folder-om-badrenovering-ABK.pdf)
[{{< mdi file-pdf >}} Folder (english)](/pdf/bathroom-renovation-Folder-om-badrenovering-ABKUK.pdf)

To see the most recent updates regarding the renovation of the bathrooms (time schedules etc.), look under the tag [\#bathroom-renovation](/tags/bathroom-renovation).

On this webpage you will find an illustration of the bathroom and a brochure about the process.

- We reserve the right to make changes to the time schedule.

- Tenants from parterre apartments will be rehoused and will be contacted in time before start-up. The rehousing will be nearby.

- Water supply in the affected building will be closed every Monday between 08.00-12.00 AM.

See also the information boards in the stairwells.
