---
title: Emergencies
bigimg:
  - src: ../../page-images/emergency.jpg
    desc: ""
---

# Fire

It is always a good idea to read the instructions that hang on the bulletin boards. At the event of a **fire**, the fire alarm should be activated. This is done by pressing on one of *red* buttons that are placed at each hallway exit - this will activate the alarm.

You then have to call the fire department (number: 1-1-2) and explain where it is burning. You can try to put out the fire by using one of the fire extinguishers that are placed in the hallways. If a fire breaks out in the *common house* you have to call the fire department (number: 1-1-2). Then all people should be evacuated, doors and windows should be closed, and the fire should be extinguished by using the fire extinguisher in the kitchen. 

If it is a *false alarm*, the alarm has to be disabled. This is done by approaching the bulletin board that hangs in the stairway, breaking the glass, and taking and turning key.

# Broken Window or Other Emergencies

In case of acute damage to the building/apartment please [contact the caretaker](/page/info/contact) if it is within working hours.

If not, contact SSG Skadesservice:

* Phone: +45 70 23 96 23
