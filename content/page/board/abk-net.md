---
title: ABK-Net
tags:
  - ABK-Net
---
ABK-Net is a union that handles the internet of ABK Kollegium. The interests of the union are handled by its committee, which consists of residents from the dormitory. The daily operation is handled by the system administrators, who can be contacted if problems arise. This website is handled by the webmasters.

## Contact

You can contact the board on Facebook Messenger by clicking the Messenger button:

{{< messengerbutton abknetforening >}}

If you prefer email, you can contact the board at [net@abk-aalborg.dk](mailto:net@abk-aalborg.dk).

## Calendar

{{< calendar 462d10e16043689583260d0f7b9f8a8c3b3ee772292090f0f5f3f603414d9fe0a3583a834046fdf7d61a3a9346e9f807 Net >}}

## Committee

Currently, the committee consists of:

| Name                 | Apartment Number | Primary Role   | Other Role(s)                    |
| -------------------- | ---------------- | -------------- | -------------------------------- |
| Jonas N. Terp        | 40               | Chairman       | Webadmin Commissioner & Sysadmin |
| Sebastian H. Hyberts | 106              | Vice Chairman  | Sysadmin                         |
| Alexander Steffensen | 170              | Treasurer      | Webmaster                        |
| Julian Jørgensen     | 172              | Vice Treasurer | Sysadmin                         |
| Mathias Andresen     | 3                | Board Member   | Webmaster                        |
| Anders Peter Aavild  | 61               | Board Member   | Webmaster & Sysadmin             |
| Lasse D. Skaalum     | 97               | Board Member   | Webmaster                        |

## Rules of Conduct

Currently, the rules of conduct are only in Danish:

{{< pdf "/media/rules-of-conduct-abk-net.pdf" >}}