---
title: Residents' council
tags:
  - ResidentsCouncil
---
The Residents' Council is there for the residents. We do what we can to make the atmosphere and daily life at the college the *best possible*, by trying to keep a close contact between the board, residents, associations at the dormitory and of course the caretaker. 

The Residents' Council consists of 7-9 unpaid members.

# Responsible For

* Taking care of good ideas from the residents
* Suggestions for improvements / new acquisitions 
* Inquiries about subsidies for associations
* Handling of complaints regarding violations of the dormitory regulations
* Opening the dormitory's two storage rooms

Lending out: 

* The common house for private events
* Urban gardens
* Sewing machine
* Jack (donkraft)
* Car ramps and more...

# Contact

In order to maintain a close contact, we urge all residents to contact us if they have questions, good ideas for initiatives at the college or generally topics they would like the board to take up.

You are *always* welcome to contact the Residents' Council. This is done by contacting one of the council's members, by meeting up to a resident council meeting or by sending a Facebook message by clicking the button below. 

{{< messengerbutton abkresidentscouncil >}}

If you prefer email, the resident council can also be contacted at [info@abk-aalborg.dk](mailto:info@abk-aalborg.dk).

It should be noted that meeting up personally at one of the members' apartment *does not* guarantee that the storage room can be opened by this kind of personal inquiry.

## Calendar

{{< calendar aa9590e0b20d202dce07054f5578e89e553022fa538841c993aac605ab4f1e4e3a60c52075e749cdd38cabaf8c6ad5e9 Beboerrådet >}}

## Members

Currently, the Residents' Council consists of, elected on Annual general meeting 2021 7. October:

**The council is currently without elected chairman.**

| Name                    | Apartment (Block) | Role                            |
| ----------------------- | ----------------- | ------------------------------- |
| Rikke E. Andersen       | 113 (5)           | Vice chairman, Acting chairman  |
| Lau Lauridsen           | 56 (7)            | Treasurer, Board representative |
| Mathias Andresen        | 3 (7)             | Member                          |
| Lauri Leemet            | 197 (3)           | Member                          |
| Lasse Damsgaard Skaalum | 97 (5)            | Member                          |
| Sazvan Salih            | 43 (7)            | Member                          |
| Kathrine Rix Sørensen   | 27 (7)            | Member                          |
| Cecilie Bang            | 17 (7)            | Member                          |
| Rasmus B. Larsen        | 182 (3)           | Member                          |
| Kristian Woxholt        | 14 (7)            | Sup.                            |
| Hans Christian Bonde    | 146 (3)           | Board representative            |

## Meetings

The Residents' Council meetings are held on the *first Tuesday of each month*, at. 17.00 in the common house and are open to all residents.

[{{< mdi folder >}}      Meeting minutes](https://drive.google.com/drive/folders/1gHe5ga6ODvqHyKyb9GrsSIEOlM26mB9m?usp=sharing) also contains general resident meetings (beboermøder)

## Rules of Procedure

Right now, the Residents' Councils articles are only available in danish, and can be read in the pdf below.

[{{< mdi file-pdf >}}      Forretningsorden (roughly translates to rules of procedure)](../../../pdf/forretningsorden.pdf)
{{< pdf "../../../pdf/forretningsorden.pdf" >}}