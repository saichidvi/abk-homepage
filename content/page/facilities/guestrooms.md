---
title: Guest Rooms
bigimg: [{src: "/media/img_20220504_081636.jpg", desc: ""}]
---

If you have guests spending the night, the dormitory has *two* guest rooms that can be rented for the purpose. The guest rooms are located in block 7 in the room next to the laundry room.

The rooms are furnished with two single beds in each room. The price for rent is **50,- kr. per night** and a deposit of **100,- kr.** (per key).

**You must bring your own pillow, duvet, pillowcase, duvet cover and bed sheet.**

# Reservation

If you want to rent one of the rooms, you must contact the caretaker, or you can send a mail to [1201vm@hasseris-boligselskab.dk](mailto:1201vm@hasseris-boligselskab.dk).

Office hours are between 07:30-08:00 and 12:30-13:00 weekdays.

The email has to contain:

* Name of resident / tenant
* Apartment number
* Dates of reservation of guest room(s).

# Payment

The payment of the *guestroom* is paid through your rent.
The payment of the *deposit* is paid when you collect the key. The amount will be refunded upon delivery of the key.

# Rules

* The guest rooms can be rented for maximum **one week**.
* The tenant is responsible for bringing his/her own bed sheets.
* The tenant handles clean-up after the stay and has to empty the waste bags.
* No smoking or pets.
* The furniture must not be removed from the guest rooms - removal will be considered theft and will be reported to the police.

{{< gallery >}}
  {{< figure src="/media/img_20220504_081636.jpg" >}}
  {{< figure src="/media/img_20220504_081658.jpg" >}}
{{< /gallery >}}