---
title: Common House - Fælleshuset
bigimg: [{src: "/images/abk-photos/commonhouse/commonhouse_hel2.jpg", desc: ""}]
---

The Common House is open for _all_ residents. You have to use your chip to get inside. It is not available when there are board meetings and other special cases.

The Common House can also be used for communal meals and "hygge" evenings.

# Inventory

The Common House has a kitchen with an oven, a big TV with cable TV, a PS4, many board games, a pool table, and a foosball table. 

{{< gallery >}}
  {{< figure src="/images/abk-photos/commonhouse/fælleshus.jpg" >}}
  {{< figure src="/images/abk-photos/commonhouse/fælleshus2.jpg" >}}
  {{< figure src="/images/abk-photos/commonhouse/fælleshus_boardgames.jpg" >}}
  {{< figure src="/images/abk-photos/commonhouse/fælleshus_køkken.jpg" >}}
{{< /gallery >}}

# Friday Bar

The [ABK-Bar](/page/board/bar/) also hosts Friday bars in the common house most Fridays where they serve cheap beers and drinks.

# Using the Common House

Under normal circumstances, one or more residents cannot use the common house to host events, where 5 or more non-residents are participating, as described in the common house regulations.

If you use the Common House, it is your responsibility to clean up afterwards. Linked below is a list of the points which the condition of the common house will be judged from.

[{{< mdi file-pdf >}} Common House Cleaning Guide](/pdf/commonhouse-cleanup.pdf)
{{< pdf "/pdf/commonhouse-cleanup.pdf" >}}

Furthermore, using the form linked below, you can seek extraordinary exemption from the rule within the guidelines and conditions described on page 2 of the form.

[{{< mdi file-pdf >}} Common House Dispensation Application](/pdf/commonhouse-application.pdf)
{{< pdf "/pdf/commonhouse-application.pdf" >}}

You send in a request by filling out the form, and send it to the [Residents' Council](/page/board/council/) via email at [info@abk-aalborg.dk](mailto:info@abk-aalborg.dk) or on Facebook.

{{< messengerbutton abkresidentscouncil >}}
