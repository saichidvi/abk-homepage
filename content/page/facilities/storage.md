---
title: Storage Rooms
bigimg: [{src: "../../page-images/box.jpg", desc: ""}]
---

The Storage Rooms are locked and can only be opened by members from the Residents' Council.

It will be possible to have the Storage Rooms opened every Tuesday at 19:00, but only with agreement from the Residents' Council.

At the agreed time you must have brought your stuff in the basement under block 5. All things must be marked with full name, apartment number and a phone number on which you can be contacted.

The Residents' Council can be contacted by clicking the button below:

{{< messengerbutton abkresidentscouncil >}}
