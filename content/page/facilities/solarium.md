---
title: Solarium
bigimg: [{src: "../../page-images/solarium.jpg", desc: ""}]
---

You can find the solarium in the basement under block 5 / common house.

For _10,- kr._ you will get _20_ minutes of sun. You pay with your chip. 

Remember to __clean up__ after your self!
