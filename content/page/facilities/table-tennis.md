---
title: Table tennis table - Bordtennis bord
---

There is a table tennis table below the common house, outside the Fitness room and the Sauna. It is _free_ for all residents! Pick you favorite neighbor, and beat them in a round of ping pong. Or even better, invite the whole corridor down for a round the table. There are bats and balls free to use, but it is recommended you bring your own.

![Sauna instructions](../../page-images/abk-photos/bordtennis.jpg)
