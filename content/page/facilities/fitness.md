---
title: Fitness Room
bigimg: [{src: "/images/abk-photos/fitness/fitness.jpg", desc: ""}]
---

In the basement under the common house/block 5 you will find the Fitness room, which is _free_ to use for residents. 
In order to access the Fitness Room, you have to use your chip. 

# Weight and Cardio Rooms

There are two rooms in the Fitness Area: The Weight Room, and the Cardio Room. 

__The Weight Room__:

- Free-weights
- Squat racks
- A bench
- Other related items

__The Cardio Room__:

- Rowing machine
- Treadmill
- Elliptical trainer/cross-trainer
- Exercise bike 
- Therapy balls 
- Body bars
- Jump ropes
- Rubber bands
- Back trainer
- Other related items. 

{{< gallery >}}
  {{< figure src="/images/abk-photos/fitness/fitness.jpg" >}}
  {{< figure src="/images/abk-photos/fitness/fitness2.jpg" >}}
  {{< figure src="/images/abk-photos/fitness/fitness3.jpg" >}}
  {{< figure src="/images/abk-photos/fitness/fitness4.jpg" >}}
{{< /gallery >}}

# New Equipment

New equipment is sometimes added, and you are welcome to contact the Residents' Council if something is missing, defective, or you have ideas or wishes for improvement. The more detailed you are, the better.

# Security

Note that it is _logged_ who uses the exercise room to secure theft and vandalism.

# Rules of Conduct

Currently, the rules of conduct are only in Danish:

Vi beder dig om at følge vores ordensregler - det sikrer en optimal træningsoplevelse for alle.

### ANSVAR

Brug af ABK-motionsrum er på eget ansvar. Motionsrummet må benyttes af alle beboere på kollegiet. Dog skal beboere under 16 år være i selskab med en voksen (over 18 år).

### OPFØRSEL

Vi ønsker, at alle skal have en god oplevelse, når de træner i ABK-motionsrum. Derfor forventer vi en positiv omgangstone med respekt for hinanden.

### BEHANDLING AF UDSTYR

- Vores udstyr skal behandles med respekt og må kun benyttes til de øvelser, det er beregnet til. Håndvægte skal have gulvkontakt, før man slipper.
- Husk altid at lægge udstyret på plads efter brug.
- Udstyr må ikke flyttes fra deres respektive pladser – de skal blive i det rum, hvortil de er beregnet, altså styrke- eller kardiorummet.
- Del maskinen i pausen mellem sættene, så bliver det nemmere for alle at komme til.
- Maskiner skal rengøres efter brug. Rengøringsartikler findes ved indgangen til AKB-motionsrummet.

### BEKLÆDNING

Du skal være iført træningstøj og rene sko. Udendørs sko er ikke tilladt!
