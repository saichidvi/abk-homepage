---
title : Tour de Chambres
---

# What is TDC?
Tour de Chambres (or abbreviated TDC) is a tradition for almost all dormitories. Here at ABK it happens that residents visit each other's apartments and get a 'small one'. The purpose of a TDC is to _get to know your neighbors better_, get around each other's rooms, consume a lot of alcohol and generally just party. There are around 1-2 __TDCs__ each year. 

# What happens in TDC?
A TDC usually starts with a _common_ dinner where we eat food together while the first drinks are consumed. Subsequently, we split the participants into groups and figure out which order we visit each other. The rest of the evening then goes to visit the individual participants' rooms, where each participant serves a drink equal to _approx. 1 item per person_ to the other participants.

Once all rooms have been reached, there is a tradition that those who have stayed out all the way gather and parties on in the common house, where the bar will be open.

# Theme
Traditionally, TDC often has an overall theme such as different movies, music genres, sports, countries or the like. Here, the individual participants can be dressed appropriately to the theme, play appropriate music in the room and possibly have the flavors and drinks to suit the theme.

An example can be a sports theme where a participant chooses to host the football theme. Here, the costume can be a player jersey, "klaphat" etc. and in the room you can hang the Denmark flag, play Re-Sepp-Ten, and possibly show football on the TV.

However, it is up to you as a participant how much time you spend on your theme, but the more people make of it, the more fun it is for everyone.

# In Danish
Tour de Chambres (eller forkortet TDC) er en tradition på næsten alle kollegier. Her på ABK foregår det ved at beboere besøger hinandes lejligheder og får en 'lille en'. Formålet med en TDC er at lære sine naboer bedre at kende, komme rundt på hinandens værelser, indtage en masse alkohol og generelt bare feste.

En TDC starter som regel med, at de beboere, som har valgt at deltage, laver og spiser mad sammen, mens de første genstande indtages. Herefter udarbejdes en liste over, hvilken rækkefølge værelserne på gangen skal besøges i, oftest via lodtrækning og ellers efter beboernes anciennitet. Resten af aftenen går så med at besøge de enkelte deltageres værelser, hvor hver deltager serverer en drink svarende til ca. 1 genstand pr. person til de øvrige deltagere.

Når alle værelser har været nået, er der tradition for, at de som har holdt ud hele vejen samles og drikker videre, f.eks. i kollegiets bar; ABK-Baren.

Traditionen tro har TDC  ofte have et overordnet tema, som f.eks. forskellige film, musikgenrer, sportsgrene, lande eller lign.. Her kan de enkelte deltagere f.eks. være klædt passende ud til temaet, spille passende musik på værelset og evt. have oppyntning og drinks som passer til temaet.

Et eksempel kan være sportstema hvor en deltager vælger at holde fodboldtema. Her kan udklædningen være en spillertrøje, klaphat osv. og på værelset kan der så hænge danmarksflag, køre Re-Sepp-Ten på anlægget, og evt. blive vist fodbold på TV'et.

Det er dog oftest valgfrit, hvor meget man som deltager vil gå op i sit tema, men jo mere folk gør ud af det, jo sjovere er det for alle.

TDC arrangeres af hver enkelt gang ofte med opfordring af gangformænden eller øvrige beboere.
