Arbejderbevægelsens Kollegium is located on the top of _Sohngårdsholmsbakken_, which is a nice and quiet area and right next to the beautiful _Sohngårdsholms Slotspark_. It is close to the city center and AAU Campus which makes the area attractive for students.

Address: Borgmester Jørgensensvej 3 -7, 9000 Aalborg
